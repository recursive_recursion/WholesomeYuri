# WholesomeYuri

List of wholesome yuri content in markdown formatting

<strong>To update:</strong> import the .jex file into [Joplin notes](https://github.com/laurent22/joplin/) with the [Ez Table v1.0.2](https://github.com/kensam94/joplin-plugin-eztable) plugin enabled.

By right-clicking the note you can export into a handy pdf, markdown, or html file!